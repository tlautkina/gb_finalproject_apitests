Автоматизация тестирования API.

ТЗ проекта:
https://docs.google.com/document/d/19IDnFaUDtxTRbvBo-EN5maXb2bkNlmYEBmtJb8A58aE/edit?usp=sharing

Задание 1. Написание автотестов. 

Ч1. Автоматизируйте POST https://test-stand.gb.ru/gateway/login (минимум 3 кейса, например передать валидные/невалидные логин и пароль и т.д), используя rest-assured.

Результат -  написаны автотесты:

1. Authorization with valid login and valid password.

2. Authorization with valid login and invalid password.

3. Authorization with invalid login and valid password.

4. Authorization with invalid login and invalid password.

5. Authorization with empty login and empty password.

6. Authorization with empty login and valid password.

7. Authorization with valid login and empty password.

8. Authorization with valid login and valid password by GET method.

9. Get and show on the screen the token of the authorized user.


Ч2. Автоматизируйте GET
https://test-stand.gb.ru/api/posts?sort=createdAt&order=ASC&page=1 - запрос со своими постами (минимум 5 кейсов, например, передав различные данные в query параметры, учтите, что неавторизованный пользователь не может запрашивать ленту с постами), используя rest-assured.

Результат -  написаны автотесты:

1. Get user's posts without query parameters.

2. Request in ascending order.

3. Request of user's posts in descending order.

4. Request of user's first page with 10 posts.

5. Request of user's zero page.

6. Request of user's non-existing page.

7. Request of user's posts with all parameters.

8. Request of user's posts without token.

9. Request of user's posts without token.

10. Request of user's posts by POST method.


Ч3. Автоматизируйте GET
https://test-stand.gb.ru/api/posts?owner=notMe&sort=createdAt&order=ASC&page=1 - запрос с чужими постами (минимум 5 кейсов, например, передав различные данные в query параметры, учтите, что неавторизованный пользователь не может запрашивать ленту с постами), используя rest-assured.

Результат -  написаны автотесты:

1. Request of all users posts.

2. Request of all user posts in ascending order.

3. Request of all user posts in descending order.

4. Request of all users first page with 4 posts.

5. Request of all users zero page.

6. Request of non-existing page of all users.

7.  Request of all posts with all parameters.

8. Request of all posts without token.

9. Request of all posts with invalid token.

10. Request of all posts by POST method.


Задание 2. Статистика по упавшим тестам

Если у вас упали автотесты, проанализируйте почему и напишите причину падения: 
упал автотест №4 из MyPostTest, который проверяет отображение кол-ва постов пользователя на одной странице (по ТЗ = 10), на странице их по факту 4. После исправления бага, автотест должен работать корректно.

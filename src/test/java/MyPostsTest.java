import io.restassured.path.json.JsonPath;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MyPostsTest extends AbstractTest {

    @DisplayName("1. Get user's posts without query parameters")
    @Test
    void getMyPostsWithoutQueryParams() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                //.log().all()
                .assertThat()
                .body("meta.nextPage", equalTo(2))
                .body("meta.prevPage", equalTo(1))
                .body("data.size()", equalTo(4));
    }

    @SneakyThrows
    @DisplayName("2. Request in ascending order")
       @Test
    void getMyPostsSortedASC() {
        JsonPath jsonPath = given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .extract().jsonPath();
        String date1 = jsonPath.get("data[0].createdAt");
        String date2 = jsonPath.get("data[1].createdAt");
        assertTrue(simpleDateFormat.parse(date1).getTime() < simpleDateFormat.parse(date2).getTime(), "Posts should be in ascending order");
    }

    @SneakyThrows
    @DisplayName("3. Request of user's posts in descending order")
    @Test
    void getMyPostsSortedDESC() {
        JsonPath jsonPath = given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .extract().jsonPath();
        String date1 = jsonPath.get("data[0].createdAt");
        String date2 = jsonPath.get("data[1].createdAt");
        assertTrue(simpleDateFormat.parse(date1).getTime() > simpleDateFormat.parse(date2).getTime(), "Posts should be in descending order");
    }

    @DisplayName("4. Request of user's first page with 10 posts")
    @Test
    void getMyPostsPage1() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("page", "1")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .assertThat()
                .body("meta.nextPage", equalTo(2))
                .body("meta.prevPage", equalTo(1)) // prevPage should be 0
                .body("data.size()", equalTo(10));
    }

    @DisplayName("5. Request of user's zero page")
    @Test
    void getMyPostsPageZero() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("page", "0")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .assertThat()
                .body("meta.prevPage", equalTo(1)) // prevPage should be null
                .body("meta.nextPage", equalTo(1));

    }

    @DisplayName("6. Request of user's non-existing page")
    @Test
    void getMyPostsPageNotExist() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("page", "100")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .assertThat()
                .body("meta.nextPage", equalTo(null))
                .body("meta.prevPage", equalTo(99));
    }

    @DisplayName("7. Request of user's posts with all parameters")
    @Test
    void getMyPostsWithAllQueryParams() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("page", "1")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                //.log().all()
                .assertThat()
                .body("meta.nextPage", equalTo(2))
                .body("data.size()", not(equalTo(0)));
    }

    @DisplayName("8. Request of user's posts without token")
    @Test
    void getMyPostsWithoutToken() {
        given()
                //.header("X-Auth-Token", getXAuthToken())
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(401);
    }

    @DisplayName("9. Request of user's posts with invalid token")
    @Test
    void getMyPostsWithInvalidToken() {
        given()
                .header("X-Auth-Token", "test123321test")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(401);
    }

    @DisplayName("10. Request of user's posts by POST method")
    @Test
    void getMyPostsMethodPOST() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .post(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(500);
    }
}
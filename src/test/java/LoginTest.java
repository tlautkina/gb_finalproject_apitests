import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LoginTest extends AbstractTest {

    @DisplayName("1. Authorization with valid login and valid password")
    @Test
    void loginValidUsernameValidPassword() {
        JsonPath jsonPath = given()
                .contentType("multipart/form-data")
                .multiPart("username", getUsername())
                .multiPart("password", getPassword())
                .when()
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(200)
                .extract().jsonPath();
        assertThat(jsonPath.get("username"), equalTo(getUsername()));
    }

    @DisplayName("2. Authorization with valid login and invalid password")
    @Test
    void loginValidUsernameInvalidPassword() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", getUsername())
                .multiPart("password", "1234")
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("error", equalTo("Invalid credentials."));
    }

    @DisplayName("3. Authorization with invalid login and valid password")
    @Test
    void loginInvalidUsernameValidPassword() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "usernotexist")
                .multiPart("password", getPassword())
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("error", equalTo("Invalid credentials."));
    }

    @DisplayName("4. Authorization with invalid login and invalid password")
    @Test
    void loginInvalidUsernameInvalidPassword() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "usernotexist")
                .multiPart("password", "1234")
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("error", equalTo("Invalid credentials."));
    }

    @DisplayName("5. Authorization with empty login and empty password")
    @Test
    void loginEmptyUsernameEmptyPassword() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "")
                .multiPart("password", "")
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("error", equalTo("Invalid credentials."));
    }

    @DisplayName("6. Authorization with empty login and valid password")
    @Test
    void loginEmptyUsernameValidPassword() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "")
                .multiPart("password", getPassword())
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("error", equalTo("Invalid credentials."));
    }

    @DisplayName("7. Authorization with valid login and empty password")
    @Test
    void loginValidUsernameEmptyPassword() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", getUsername())
                .multiPart("password", "")
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("error", equalTo("Invalid credentials."));
    }

    @DisplayName("8. Authorization with valid login and valid password by GET method")
    @Test
    void loginValidUsernameValidPasswordMethodGET() {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", getUsername())
                .multiPart("password", getPassword())
                .get(getBaseUrl() + "/gateway/login")
                .then()
                .assertThat()
                .statusCode(400);
    }

    @DisplayName("9. Get and show on the screen the token of the authorized user")
    @Test
    void getToken() {
        String token = given()
                .contentType("multipart/form-data")
                .multiPart("username", getUsername())
                .multiPart("password", getPassword())
                .post(getBaseUrl() + "/gateway/login")
                .then()
                .statusCode(200)
                .extract().jsonPath()
                .get("token").toString();
        System.out.println("token: " + token);
    }

}
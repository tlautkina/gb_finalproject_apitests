import io.restassured.path.json.JsonPath;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OtherPostsTest extends AbstractTest {

    @DisplayName("1. Request of all users posts")
    @Test
    void getOtherPosts() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                //.log().all()
                .assertThat()
                .body("meta.nextPage", equalTo(2))
                .body("meta.prevPage", equalTo(1))
                .body("data.size()", equalTo(4));
    }


    @SneakyThrows
    @DisplayName("2. Request of all user posts in ascending order")
    @Test
    void getOtherPostsSortedASC() {
        JsonPath jsonPath = given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .extract().jsonPath();
        String date1 = jsonPath.get("data[0].createdAt");
        String date2 = jsonPath.get("data[1].createdAt");
        assertTrue(simpleDateFormat.parse(date1).getTime() < simpleDateFormat.parse(date2).getTime(), "Posts should be in ascending order");
    }

    @SneakyThrows
    @DisplayName("3. Request of all user posts in descending order")
    @Test
    void getOtherPostsSortedDESC() {
        JsonPath jsonPath = given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .assertThat()
                .statusCode(200)
                .extract().jsonPath();
        String date1 = jsonPath.get("data[0].createdAt");
        String date2 = jsonPath.get("data[3].createdAt");
        assertTrue(simpleDateFormat.parse(date1).getTime() > simpleDateFormat.parse(date2).getTime(), "Posts should be in descending order");
    }

    @DisplayName("4. Request of all users first page with 4 posts")
    @Test
    void getOtherPostsPage1() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .queryParam("page", "1")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .log().all()
                .assertThat()
                .body("meta.nextPage", equalTo(2))
                .body("meta.prevPage", equalTo(1)) // prevPage should be 0
                .body("data.size()", equalTo(4));
    }

    @DisplayName("5. Request of all users zero page")
    @Test
    void getOtherPostsPageZero() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .queryParam("page", "0")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .assertThat()
                .body("meta.nextPage", equalTo(1))
                .body("meta.prevPage", equalTo(1)); // prevPage should be null
    }

    @DisplayName("6. Request of non-existing page of all users")
    @Test
    void getOtherPostsPageNotExist() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .queryParam("page", "10000")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                .assertThat()
                .body("meta.nextPage", equalTo(null))
                .body("meta.prevPage", equalTo(9999));
    }

    @DisplayName("7.  Request of all posts with all parameters")
    @Test
    void getOtherPostsWithAllQueryParams() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .queryParam("page", "1")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(200)
                //.log().all()
                .assertThat()
                .body("meta.nextPage", equalTo(2))
                .body("data.size()", not(equalTo(0)));
    }

    @DisplayName("8. Request of all posts without token")
    @Test
    void getOtherPostWithoutToken() {
        given()
                .queryParam("owner", "notMe")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(401);
    }

    @DisplayName("9. Request of all posts with invalid token")
    @Test
    void getOtherPostsWithInvalidToken() {
        given()
                .header("X-Auth-Token", "test123321test")
                .queryParam("owner", "notMe")
                .get(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(401);
    }

    @DisplayName("10. Request of all posts by POST method")
    @Test
    void getOtherPostsMethodPOST() {
        given()
                .header("X-Auth-Token", getXAuthToken())
                .queryParam("owner", "notMe")
                .post(getBaseUrl() + "/api/posts")
                .then()
                .statusCode(500);
    }
}